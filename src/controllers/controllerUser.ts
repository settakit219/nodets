import { Request, Response, NextFunction } from 'express'

import service from '../databases/dbUser'
import { User, ResUser, ResponseUser} from '../interfaces/userInterface'
import { encryptPasswrod, comparePassword } from '../helpers/password'
import { gennerateUserId, gennerateRefreshToken, gennerateAccessToken } from '../helpers/generator'


// declare global {
//     namespace Express {
//         export interface Request {
//             user: ResUser;
//         }
//     }
// }

export interface test extends Request{
    user1: {
        id : 1,
        username : "test"
    }
}

const jsonMessage = {
    status: 0,
    message: 'Internal Server Error'
}

const getAllUser = async (req: Request, res: Response) => {
    let result = await service.getAllUser()
    res.status(200).json(result)
}

const createUser = async (req: Request, res: Response) => {
    let password = encryptPasswrod(req.body.password);
    let user: User = {
        id: gennerateUserId(),
        username: req.body.username,
        password,
        email: req.body.email
    }
    try {
        let result = await service.createUser(user)
        jsonMessage.status = 1
        jsonMessage.message = 'success'
        res.status(200).json(jsonMessage)
    } catch (err) {
        jsonMessage.message = err
        res.status(500).json(jsonMessage)
    }
}

const loginUser = async (req: Request, res: Response) => {
    try {
        let result = await service.userLogin(req.body.username);
        if (result.length <= 0) {
            jsonMessage.message = 'username not found'
            return res.status(400).json(jsonMessage)
        }
        let passwordCheck = comparePassword(req.body.password, result[0].password)
        if (!passwordCheck) {
            jsonMessage.message = 'password incorrect'
            return res.status(400).json(jsonMessage)
        }
        let data = {
            username: result[0].username,
            email: result[0].email,
            token: {
                refreshToken: gennerateRefreshToken(result[0].id),
                accessToken: gennerateAccessToken(result[0].id)
            }
        }
        res.status(200).json({ status: 1, message: 'login success', data: data })
    } catch (err) {
        jsonMessage.message = err
        res.status(500).json(jsonMessage)
    }
}

const renewToken = (req: test, res: Response) => {
    console.log(req.user1.username)
    const abc = req.user
    res.status(200).json('OK')
}



export default { getAllUser, createUser, loginUser , renewToken}