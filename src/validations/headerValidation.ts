import { header, validationResult } from "express-validator";
import { Request, Response, NextFunction } from "express";

export const authToken = [
    header('authorization')
        .exists().withMessage('missing authorization key'),
    (req: Request, res: Response, next: NextFunction) => {
        let errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                status: 0,
                message: errors.array()
            })
        }
        next()
    }
]