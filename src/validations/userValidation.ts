import { body, validationResult } from 'express-validator'
import { Request, Response, NextFunction } from 'express'

const createUser = [
    body('username')
        .exists().withMessage('username is require').bail()
        .isLength({ min: 3 }).withMessage('username have at minimuum 3 character').bail()
        .isLength({ max: 20 }).withMessage('username have at maximum 20 character').bail(),
    body('password')
        .exists().withMessage('Password is require').bail()
        .isLength({ min: 8 }).withMessage('password have at minimum 8 character').bail()
        .matches(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])/).withMessage('password formate incorrect').bail(),
    body('email')
        .exists().withMessage('email is require'),
    (req: Request, res: Response, next: NextFunction) => {
        let errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({ status: 0, message: errors.array() })
        }
        next()
    }
]

const loginUser = [
    body('username')
        .exists().withMessage('username is require'),
    body('password')
        .exists().withMessage('Password is require'),
    (req: Request, res: Response, next: NextFunction) => {
        let errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({ status: 0, message: errors.array() })
        }
        next()
    }
]

export default { createUser, loginUser }