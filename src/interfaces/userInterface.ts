import { type } from "os";

export interface CreateUser {
    username: string;
    password: string;
    email: string;
}

export interface User extends CreateUser {
    id: string;
}

export interface ResUser {
    id: string;
    username: string;
    email: string;
}

export interface ResponseUser extends Express.Request {
    user: ResUser;
}