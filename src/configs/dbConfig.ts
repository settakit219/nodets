import { Pool } from 'pg'
import config from '../configs/config'

const pool = new Pool({
    user: config.dbUsername,
    password: config.dbPassword,
    host: config.dbHost,
    port: parseInt(config.dbPort),
    database: config.dbName
})

export default pool