import * as dotenv from 'dotenv'
import path from 'path'

const root = process.cwd()


if (process.env.NODE_ENV !== 'production') {
    dotenv.config({ path: `${path.join(root, '.env')}` })
}

dotenv.config({ path: `${path.join(root, '.env.production')}` })


export default {
    port: process.env.PORT as string,
    host: process.env.HOST as string,
    dbHost: process.env.DB_HOST as string,
    dbPort: process.env.DB_PORT as string,
    dbName: process.env.DB_NAME as string,
    dbUsername: process.env.DB_USERNAME as string,
    dbPassword: process.env.DB_PASSWORD as string,
    secretKey: 'mykey'
}