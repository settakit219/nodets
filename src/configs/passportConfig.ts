import { ExtractJwt, Strategy } from 'passport-jwt'

import config from '../configs/config'
import service from '../databases/dbUser'

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.secretKey
}

export const jwtAuth = new Strategy(jwtOptions, (payload, done) => {
    service.findUserById(payload.data).then(result => {
        if(result.length <= 0){
            return done(null, false);
        }
        done(null, result)
    })
    .catch(err => {
        console.log(err);
        return done(err, false)
    })
})

