import * as rfs from 'rotating-file-stream'
import path from 'path'

const accessLogStream = rfs.createStream('access.log', {
    interval: '3M', // rotate daily
    path: path.join(`${process.cwd()}/src`, 'logs')
})


export default accessLogStream