import { Router } from 'express'
import passport from 'passport'

import controller from '../controllers/controllerUser'
import validator from '../validations/userValidation'
import { jwtAuth } from '../configs/passportConfig'
import { authentication } from '../middlewares/auth'
import { authToken } from '../validations/headerValidation'

const userRouter: Router = Router()

passport.initialize()
passport.use(jwtAuth)

userRouter.get('/', authToken, authentication,  controller.getAllUser)
userRouter.post('/', validator.createUser, controller.createUser)
userRouter.post('/login', validator.loginUser, controller.loginUser)

export default userRouter