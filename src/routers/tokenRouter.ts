import { Router } from 'express'
import passport from 'passport'

import { jwtAuth } from '../configs/passportConfig'
import * as validate from '../validations/headerValidation'
import { authentication } from '../middlewares/auth'
import controller from '../controllers/controllerUser'

const tokenRouter = Router()

passport.initialize()
passport.use(jwtAuth)

tokenRouter.get('/renewToken', validate.authToken, authentication, controller.renewToken)

export default tokenRouter

