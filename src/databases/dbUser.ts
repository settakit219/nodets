import pool from '../configs/dbConfig'
import { User, ResponseUser }  from '../interfaces/userInterface'

const createUser = (user: User) => {
    return new Promise((resolve, reject) => {
        let query = `INSERT INTO person VALUES ('${user.id}', '${user.username}', '${user.password}', '${user.email}')`
        pool.query(query, (err, result) => {
            if (err) {
                return reject(err.message)
            }
            resolve(result.rowCount)
        })
    })
}

const getAllUser = () => {
    return new Promise<User[]>((resolve, reject) => {
        let query = `SELECT * FROM person`
        pool.query(query, (err, result) => {
            if (err) {
                console.log(err.message)
                return reject(err.message)
            }
            resolve(result.rows)
        })
    })
}

const userLogin = (username: string) => {
    return new Promise<User[]>((resolve, reject) => {
        let query = `SELECT id, username, password, email FROM person WHERE username = '${username}'`
        pool.query(query, (err, result) => {
            if (err) {
                console.log(err.message)
                return reject(err.message)
            }
            resolve(result.rows)
        })
    })
}

const findUserById = (id: string) => {
    return new Promise<ResponseUser[]>((resolve, reject) => {
        let query = `SELECT id, username, email FROM person WHERE id = '${id}'`
        pool.query(query, (err, result) => {
            if (err) {
                console.log(err.message)
                return reject(err.message)
            }
            resolve(result.rows)
        })
    })
}

export default { getAllUser, createUser, userLogin, findUserById }