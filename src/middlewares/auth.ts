import passport from 'passport'
import { Request, Response, NextFunction } from 'express'
import { ResponseUser } from '../interfaces/userInterface'
import { test } from '../controllers/controllerUser'
 
export const authentication = (req: test, res: Response, next: NextFunction) => {
    return passport.authenticate('jwt', {
        session: false
    }, (err, user, info) => {
        if (err) {
            return next(err)
        }
        if (!user) {
            return res.status(401).json({
                status: 0,
                message: 'Uanothorized'
            })
        }
        req.user1 = user
        next();
    })(req, res, next)
}