import jwt from 'jsonwebtoken'

import config from '../configs/config'

export const gennerateUserId = () => {
    let year = new Date().getTime().toString()
    return 'u' + year
}

export const gennerateAccessToken = (id: string) => {
    return jwt.sign({ data: id }, config.secretKey, { expiresIn: '1h' })
}

export const gennerateRefreshToken = (id: string) => {
    return jwt.sign({ data: id }, config.secretKey, { expiresIn: '72h' })
}