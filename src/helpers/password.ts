import bcrypt from 'bcrypt'

export const encryptPasswrod = (password: string) => {
    let salt = Math.floor(Math.random() * 10) + 1
    let hashPassword = bcrypt.hashSync(password, salt);
    return hashPassword;
}

export const comparePassword = (password: string, hashPassword: string) => {
    return bcrypt.compareSync(password, hashPassword);
}