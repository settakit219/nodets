import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'

import accessLogStream from './configs/logsConfig'
import config from './configs/config'
import userRouter from './routers/userRouter'
import tokenRouter from './routers/tokenRouter'

const app: express.Application = express()
const port = parseInt(config.port)
const host = config.host


app.use(morgan('common', { stream: accessLogStream }))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/user', userRouter)
app.use('/token', tokenRouter)

app.listen(port, host, () => {
    console.log(`server running... on ${host}:${port}`)
})