"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dbConfig_1 = __importDefault(require("../configs/dbConfig"));
var createUser = function (user) {
    return new Promise(function (resolve, reject) {
        var query = "INSERT INTO person VALUES ('" + user.id + "', '" + user.username + "', '" + user.password + "', '" + user.email + "')";
        dbConfig_1.default.query(query, function (err, result) {
            if (err) {
                return reject(err.message);
            }
            resolve(result.rowCount);
        });
    });
};
var getAllUser = function () {
    return new Promise(function (resolve, reject) {
        var query = "SELECT * FROM person";
        dbConfig_1.default.query(query, function (err, result) {
            if (err) {
                console.log(err.message);
                return reject(err.message);
            }
            resolve(result.rows);
        });
    });
};
var userLogin = function (username) {
    return new Promise(function (resolve, reject) {
        var query = "SELECT id, username, password, email FROM person WHERE username = '" + username + "'";
        dbConfig_1.default.query(query, function (err, result) {
            if (err) {
                console.log(err.message);
                return reject(err.message);
            }
            resolve(result.rows);
        });
    });
};
exports.default = { getAllUser: getAllUser, createUser: createUser, userLogin: userLogin };
