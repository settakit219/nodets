"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_validator_1 = require("express-validator");
var createUser = [
    express_validator_1.body('username')
        .exists().withMessage('username is require').bail()
        .isLength({ min: 3 }).withMessage('username have at minimuum 3 character').bail()
        .isLength({ max: 20 }).withMessage('username have at maximum 20 character').bail(),
    express_validator_1.body('password')
        .exists().withMessage('Password is require').bail()
        .isLength({ min: 8 }).withMessage('password have at minimum 8 character').bail()
        .matches(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])/).withMessage('password formate incorrect').bail(),
    express_validator_1.body('email')
        .exists().withMessage('email is require'),
    function (req, res, next) {
        var errors = express_validator_1.validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ status: 0, message: errors.array() });
        }
        next();
    }
];
var loginUser = [
    express_validator_1.body('username')
        .exists().withMessage('username is require'),
    express_validator_1.body('password')
        .exists().withMessage('Password is require'),
    function (req, res, next) {
        var errors = express_validator_1.validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ status: 0, message: errors.array() });
        }
        next();
    }
];
exports.default = { createUser: createUser, loginUser: loginUser };
