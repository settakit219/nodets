"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var morgan_1 = __importDefault(require("morgan"));
var logsConfig_1 = __importDefault(require("./configs/logsConfig"));
var config_1 = __importDefault(require("./configs/config"));
var userRouter_1 = __importDefault(require("./routers/userRouter"));
var app = express_1.default();
var port = parseInt(config_1.default.port);
var host = config_1.default.host;
app.use(morgan_1.default('common', { stream: logsConfig_1.default }));
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.use('/user', userRouter_1.default);
app.listen(port, host, function () {
    console.log("server running... on " + host + ":" + port);
});
