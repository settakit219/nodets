"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var controllerUser_1 = __importDefault(require("../controllers/controllerUser"));
var userValidation_1 = __importDefault(require("../validations/userValidation"));
var userRouter = express_1.Router();
userRouter.get('/', controllerUser_1.default.getAllUser);
userRouter.post('/', userValidation_1.default.createUser, controllerUser_1.default.createUser);
userRouter.post('/login', userValidation_1.default.loginUser, controllerUser_1.default.loginUser);
exports.default = userRouter;
