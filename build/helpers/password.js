"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.comparePassword = exports.encryptPasswrod = void 0;
var bcrypt_1 = __importDefault(require("bcrypt"));
exports.encryptPasswrod = function (password) {
    var salt = Math.floor(Math.random() * 10) + 1;
    var hashPassword = bcrypt_1.default.hashSync(password, salt);
    return hashPassword;
};
exports.comparePassword = function (password, hashPassword) {
    return bcrypt_1.default.compareSync(password, hashPassword);
};
