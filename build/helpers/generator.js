"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.gennerateRefreshToken = exports.gennerateAccessToken = exports.gennerateUserId = void 0;
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var config_1 = __importDefault(require("../configs/config"));
exports.gennerateUserId = function () {
    var year = new Date().getTime().toString();
    return 'u' + year;
};
exports.gennerateAccessToken = function (id) {
    return jsonwebtoken_1.default.sign({ data: id }, config_1.default.secretKey, { expiresIn: '1h' });
};
exports.gennerateRefreshToken = function (id) {
    return jsonwebtoken_1.default.sign({ data: id }, config_1.default.secretKey, { expiresIn: '72h' });
};
