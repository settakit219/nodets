"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var pg_1 = require("pg");
var config_1 = __importDefault(require("../configs/config"));
var pool = new pg_1.Pool({
    user: config_1.default.dbUsername,
    password: config_1.default.dbPassword,
    host: config_1.default.dbHost,
    port: parseInt(config_1.default.dbPort),
    database: config_1.default.dbName
});
exports.default = pool;
